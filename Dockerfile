FROM composer:2.8.0 as build

ADD . /build
WORKDIR /build
RUN /build/install.sh

FROM registry.git.autistici.org/ai3/tools/modsec-logger:master AS modseclogger

FROM registry.git.autistici.org/ai3/docker/apache2-php-base:bookworm

COPY --from=build /build/app/ /opt/noblogs/www

COPY docker/htaccess /opt/noblogs/www/.htaccess
COPY docker/htaccess-noindex /opt/noblogs/www/wp-admin/.htaccess
COPY docker/htaccess-noindex /opt/noblogs/www/wp-includes/.htaccess
COPY docker/wp-config.php /opt/noblogs/www/wp-config.php
COPY docker/wp-cache-config.php /opt/noblogs/www/wp-content/wp-cache-config.php
COPY docker/conf /tmp/conf
COPY docker/build.sh /tmp/build.sh
COPY --from=modseclogger /modsec-logger /usr/local/bin/modsec-logger

# Install wp-cli in /usr/local/bin (aliased as "wp").
ADD https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar /usr/local/bin/wp-cli.phar
COPY docker/wp /usr/local/bin/wp
RUN chmod 0755 /usr/local/bin/wp /usr/local/bin/wp-cli.phar

RUN /tmp/build.sh && rm /tmp/build.sh

# For testing purposes (8080 is the default port of apache2-php-base).
EXPOSE 8080/tcp

