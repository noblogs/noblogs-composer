This is the codebase of Noblogs (previously in ai/noblogs-wp) managed via Composer ([e.g](https://www.smashingmagazine.com/2019/03/composer-wordpress/)), a PHP dependency manager.

# Set-up preliminare

## Installare composer

Per interagire con la configurazione di

* Instalare composer e impostare il repository

(richiede una versione di PHP moderna)

``` shell
apt install composer
```

La versione 1.10.1 sembra funzionare con le nostre impostazioni, qualcuno ha avuto problemi con la 1.8. È uscita la 2.0 ma non abbiamo fatto test al riguardo.

Io ho dovuto installare alcune librerie php extra

``` shell
apt install php-xml php-zip php-curl
```

Se non avete la versione di composer giusta (per esempio, in Debian Buster non è
sufficientemente recente), si può usare Docker per eseguire la versione giusta, per esempio con:

```shell
docker run --rm --volume $PWD:/data --workdir /data composer:1.10 composer [....]
```
Se sequite questa strada, tutti i comandi descritti qui sotto vanno lanciati in questo modo (invece che chiamando composer direttamente).

## Set up dell'autenticazione

Siccome alcune dipendenze sono al momento in repository privati,
composer ha bisogno di autenticarsi con git.autistici.org. Per fare
ciò bisogna ottenere un *token di autenticazione* (da dentro Gitlab,
User Settings > Access Tokens, selezionare *read_api*), e poi dare il
comando:

```shell
composer config gitlab-token.git.autistici.org $PERSONAL_ACCESS_TOKEN
```
Questo comando creerà un file *auth.json* con il token: questo file
verrà ignorato da git perché non va assolutamente committato nel
repository.

# Come funziona composer

In questo repository ci sono solo le patch ed i file di configuarazione di composer (al momento, ~8Mb). Il codice vero e proprio di noblogs (l'equivalente di ai/noblogs-wp> per intenderci) abbiamo deciso che non verrà mantenuto in git, dato che viene generato da composer a partire da questo repository. 

In particolare ci sono 2 file importanti:

- ```composer.json``` è un file JSON che contiene la "ricetta" di come costruire il codice di Noblogs. Questo vuol dire che contiene informazioni sulle *dipendenze* di Noblogs, quali:
  * La release di Wordpress core
  * Temi e plugins (e i mu-plugins)
  * I language pack
  * I plugins drop-in, tipo hyperdb/ludicrousdb
  * Codice che abbiamo scritto noi, r2db, etc.
  * Le patch che abbiamo deciso di applicare ai sorgenti upstream
più le informazioni su dove installare tutte queste cose. Si può modificare sia a mano che usando composer. 
- ```composer.lock``` è la versione *"compilata"* dello stesso file, ovvero dopo che composer ha controllato che la ricetta in ```composer.json``` sia effettivamente installabile.

Una volta che modifichiamo ```composer.json``` (a mano o con altri metodi), per aggiornare ```composer.lock``` bisogna lanciare il comando
```shell
composer update
```
Questo comando aggiornerà anche tutte le dipendenze alla versione più recente disponibile, a meno che non abbiamo forzato una dipendenza ad una versione specifica.
Il codice verrà scompattato nella directory ```app/``` con dentro una copia del sorgente di Noblogs, e viene aggiornato il file ```composer.lock```. A quel punto possiamo committare le modifiche a ```composer.json``` e ```composer.lock```.

Per creare il repository docker per test e produzione invece lanceremo il comando
```shell
composer install
```
che legge solo il file ```composer.lock```. Per cui se aggiorniamo ```composer.json``` ma non lanciamo il comando ```composer update``` e committiamo i cambiamenti risultanti in ```composer.lock```, le modifiche non verranno mai eseguite nella fase di build delle immagini docker.

## Come aggiungere una nuova dipendenza
Con il comando
```shell
composer require nome/della-dipendenza
```

## Da dove prendiamo le dipendenze

Composer installerà le dipendenze da quattro fonti diverse (fondamentalmente):

- Il repository di Composer https://packagist.org/ 
- Un repository che fa da mirror a Wordpress.org https://wpackagist.org/ Per cui per installare temi/plugin che sono disponibili su worpdress.org basta aggiungerli come dipendenze con la sequente convenzione
  * i temi sono sotto ```wpackagist-theme/nometema```
  * i plugin sono sotto ```wpackagist-plugin/nomeplugin```
- Un repository per assett e librerie in javascript: https://asset-packagist.org/
- Il codice scritto da noi (temi e plugin) si trova nel gruppo @noblogs. Viene tutto rilasciato come pacchetti Composer sul Registry di GitLab. Se si vuole creare un nuovo pacchetto/dipendenza, basta seguire le istruzioni in noblogs/composer-publish>. A questo punto ogni volta che pushiamo una nuova tag su gitlab verrà creata una nuova release.
Se il repository che includiamo ha delle tag di versione del tipo ```vX.Y.Z``` (dove X, Y, Z sono numeri, per esempio "v1.2" o "v4.0.1"), allora ```composer``` sa cosa fare (prendere l'ultima versione disponibile di default).


## La gestione delle patch

A volte vogliamo patchare codice presente su uno dei tre repository non gestiti da noi (dato che quello nel reposiroty  gestito da noi possiamo direttamente modificarlo).
Questa cosa si gestisce con una serie di file di patch, in ```patches```.

Vige la regola che una patch agisce **solo su una dipendenza "unica"**, quindi non si possono cambiare tutti i footer di tutti i temi in una sola patch, per esempio. L'idea è avere unità di patch piccole e abbastanza semplici da leggere. Le patch vanno aggiunte manualmente in ```composer.patch.json```.

Se si aggiunge una patch e si vuole che questa ultima venga inclusa in ```composer.lock``` ma senza aggiornare le altre dipendenze, si può usare il comando:
```shell
composer update nothing
```

# Struttura delle directory generate

- una directory ```app/``` dove troveremo tutto il codice che dovrà andare dentro il container
- una directory ```vendor/``` dove verranno spacchettate alcune dipendenze di composer di cui non abbiamo bisogno in produzione per eseguire Noblogs

Composer quando aggiorna un pacchetto, rimuove tutti i file che trova nella directory dove deve essere spacchettato, prima di mettere quelli nuovi (di modo che non rimangano file "vecchi"). Questo vuol dire che se si aggiorna wordpress-core dopo aver installato temi e pacchetti, questi ultimi vengono rimossi, **a meno che** non siano stati installati in una directory separata.
Mi sembra che di suo composer segua l'ordine dei requirement nel file json nell'ordine in cui sono scritti, per cui non è un problema se ogni volta ri-creiamo la directory ```app/``` da zero.
In altre parole, se ne teniamo una copia in locale per comodità, questa verrà svuotata nel caso di un aggiornamento a wordpress-core.


## mu-plugins (must-use plugins) e dropin

Questo due tipi di plugin sono un po' buffi: i mu-plugins sono distribuiti come i plugin normali, e purtroppo
il repository ```wpackagist``` categorizza tutti come ```wordpress-plugin```. Bisogna quindi elencarli manualmente nell'elenco dei ```drop-in``` (vedi il file ```composer.json```). Simile la situazione dei plugin dropin, tipo ```hyperdb``` e ```r2db```, dove dobbiamo specificare a mano cosa fa messo dove.b```. 

## Creare un container di sviluppo

Si può creare un container per lo sviluppo in locale con docker

```bash
docker build \
    --build-arg=uid=$(id -u) \
    --build-arg=gid=$(id -g) \
    -f Dockerfile.dev \
    -t registry.git.autistici.org/noblogs/noblogs-composer-dev \
    .
```

Per lo sviluppo poi

```
docker run \
    --rm \
    -v $PWD:$PWD \
    -w $PWD \
    -ti registry.git.autistici.org/noblogs/noblogs-composer-dev:latest 
```
