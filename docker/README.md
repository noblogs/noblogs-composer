Docker image for noblogs
========================

Assumptions:

* the MySQL database is provided externally
* wp-content/blogs.dir (local storage) is provided externally
* wp-content/cache (wp-supercache render cache) is provided externally

The idea is that we provide a single JSON configuration file, and in
return we get a container exposing a HTTP endpoint for noblogs.org.

The container is generic: secrets and other configuration parameters
are loaded from the JSON configuration file using a custom
wp-config.php.

## Disable a ModSecurity rule

For this and other customizations, just edit the contents of
conf/modsecurity/crs/RESPONSE-999-EXCLUSION-RULES-AFTER-CRS.conf and
add whatever directives are necessary.

If you just want to disable a specific rule ID (for example 123456),
something like the following will do:

```
SecRuleRemoveById 123456
```

## Whitelist a URL in ModSecurity

The syntax to whitelist a URL (i.e. disable the ModSecurity engine on
a path-based criteria) is not trivial to come up with: if you want to
whitelist /some/url.php, you'll have to modify
conf/modsecurity/local.cf and add a directive like the following:

```
SecRule REQUEST_URI "@beginsWith /some/url.php" "id:1,ctl:ruleEngine=Off"
```

Pay attention to use a new *id* (1 in the example above) and pick one
that isn't already taken, or you will create a broken Docker image that
can't start (this is a configuration syntax error).

