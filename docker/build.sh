#!/bin/sh
#
# Install script for git.autistici.org/ai/website
# inside a Docker container.
#
# The installation procedure requires installing some
# dedicated packages, so we have split it out to a script
# for legibility.

# Packages that are only used to build the site. These will be
# removed once we're done.
BUILD_PACKAGES="rsync"

# Packages required to serve the website and run the services.
# We have to keep the python3 packages around in order to run
# chaperone (installed via pip).
PACKAGES="
        libapache2-mod-xsendfile
        libapache2-mod-security2
        modsecurity-crs
        php-gd
        php-imagick
        php-intl
        php-mysql
        php-memcache
        php-memcached
        php-mbstring
        php-xml
        php-zip
        openssl

        noblogs-cli
"

# Additional Apache modules to enable.
APACHE_MODULES_ENABLE="
        expires
        headers
        rewrite
        security2
        xsendfile
"

# Additional config snippets to enable for Apache.
APACHE_CONFIG_ENABLE="
        modsecurity-custom
"

# Sites to enable.
APACHE_SITES="
        noblogs.org
        noblogs.ai-cdn.net
"

# The default bitnami/minideb image defines an 'install_packages'
# command which is just a convenient helper. Define our own in
# case we are using some other Debian image.
if [ "x$(which install_packages)" = "x" ]; then
    install_packages() {
        env DEBIAN_FRONTEND=noninteractive apt-get install -qqy --no-install-recommends "$@"
    }
fi

set -e

apt-get -q update
install_packages ${BUILD_PACKAGES} ${PACKAGES}

# Install the configuration, overlayed over /etc.
rsync -a /tmp/conf/ /etc/

# Setup apache.
a2enmod -q ${APACHE_MODULES_ENABLE}
a2enconf -q ${APACHE_CONFIG_ENABLE}
a2ensite ${APACHE_SITES}

# Set up modsecurity.
# The file is named 00modsecurity.conf so it is loaded first.
mv /etc/modsecurity/modsecurity.conf-recommended /etc/modsecurity/00modsecurity.conf

# Patch the /etc/modsecurity/00modsecurity.conf file shipped
# with the packages because it fails with a SyntaxError as
# soon as it sees the first SecAuditLog directive.
sed -e 's,^SecAuditLog .*$,SecAuditLog /dev/null,' -i \
    /etc/modsecurity/00modsecurity.conf

# This needs to be writable for mod security to be able to start.
# Useless because the float container is read-only anyway.
install -d -m 1777 /var/log/apache2

# Ensure that the mount points exist.
mkdir -p /opt/noblogs/www/wp-content/blogs.dir
mkdir -p /opt/noblogs/www/wp-content/cache
mkdir -p /var/lib/php/uploads

# Remove Akismet e HelloDolly (plugins but from wp-core)

rm -rf /opt/noblogs/www/wp-content/plugins/akismet/
rm -f /opt/noblogs/www/wp-content/plugins/hello.php

# Create a symlink for the bp-default theme.
ln -s ../plugins/buddypress/bp-themes/bp-default /opt/noblogs/www/wp-content/themes/bp-default

# Remove packages used for installation.
apt-get remove -y --purge ${BUILD_PACKAGES}
apt-get autoremove -y
apt-get clean
rm -fr /var/lib/apt/lists/*
rm -fr /tmp/conf

