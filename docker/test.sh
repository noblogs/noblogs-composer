#!/bin/sh

TESTSUITE_IMAGE="registry.git.autistici.org/noblogs/testsuite:main"

# Run a ssl-reverse-proxy in the background.
podman run -d --expose 8443 --rm --env PROXY_DOMAIN=noblogs.org --env PROXY_BACKEND_ADDR=localhost:8080 --network=host registry.git.autistici.org/pipelines/images/test/ssl-reverse-proxy:main

# Give the ssl-reverse-proxy a little time to start.
# (It would be better to probe the port)
sleep 3

# Run the test suite using Podman, in the foreground.
# Mount a local temporary directory to hold artifacts.
mkdir -p test-artifacts
podman run --rm --pull=always \
    --env TARGET_URL=${TARGET_URL} \
    --env TARGET_ADDR=${TARGET_ADDR} \
    --env SCREENSHOT_DIR=/artifacts/screenshots \
    --network=host \
    --mount type=bind,source=test-artifacts,destination=/artifacts \
    ${TESTSUITE_IMAGE} \
    --junitxml=/artifacts/pytest.xml
rc=$?

# Convert into permanent error.
if [ $rc -gt 0 ]; then
    echo "test suite exited with status $rc" >&2
    exit 2
fi

exit 0

