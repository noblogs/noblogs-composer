#!/bin/sh

set -e
umask 022

composer install --prefer-dist --no-ansi --no-progress --no-interaction

# remove useless files
find app -name 'composer.json' -delete
find app -name 'PATCHES.txt' -delete
find app -name 'readme.*' -delete
find app -name 'license.*' -delete

exit 0
